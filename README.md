# README #

### Overview ###

* The firiaxg1 library is a set of helper scripts for users of the Firia XG1 gateway
* Version 1.0.0

### Installation ###

* This library requires Python v3.7+
* This library was designed specifically to run on the Firia XG1 gateway
* The python dependencies are listed in the requirements.txt file

### Usage ###

* Multiple test / example scripts are provided at the root level
* You can run these test script from the cmd line with:  python3 xxxx_test_script.py

### License ###

* This library can be used freely to support your XG1 gateway projects
* [MIT](https://choosealicense.com/licenses/mit/)

### Contact ###

* For assistance with this library please contact info@firia.com