from time import sleep

from firiaxg1.core import init_core
from firiaxg1.cell_mdm import *

if __name__ == "__main__":

  print("Starting test")
  print("Initializing core")
  init_core()

  print("Construct Cell Modem Object")
  cm = CellModem()

  print("Turn Cell Modem Power On")
  cm.power_on()

  print("Wait 30 Seconds")
  sleep(30)

  print("Open AT Serial Port to Cell Modem")
  cm.open_at_port()

  print("Sending Basic AT Command 'AT'")
  byte_at_cmd = b'AT\r\n'
  cm.send_at_cmd(byte_at_cmd)
  sleep(0.1)
  byte_resp = cm.read_at_resp()
  print("Cell Modem Response:")
  resp_str = byte_resp.decode()
  print(resp_str)

  print("Closing AT Serial Port")
  cm.close_at_port()

  print("Unconditional Power Down - NOTE: Should only be called if Unresponsive")
  cm.unconditional_shut_down()

  print("Wait 30 Seconds")
  sleep(30)

  print("Turn Cell Modem Power On")
  cm.power_on()

  print("Wait 30 Seconds")
  sleep(30)

  print("Power Cell Modem Normal Power Down")
  cm.normal_shut_down()

  print("Wait 30 Seconds")
  sleep(30)

  print("Test Complete")
