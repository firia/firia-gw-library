from time import sleep

from datetime import datetime, timezone

import firiaxg1.core as core
from firiaxg1.rtc import *
from firiaxg1.eco import *
from firiaxg1.gpio import *

if __name__ == "__main__":
  print("Starting ECO and RTC test script")
  print("Initialize library core")
  core.init_core()

  print("Construct ECO Processor Object")
  eco = ECOProcessor(ECO_MODULE.MGM12P)

  print("Bitbang the Unlock / Mass Erase Cmd to ECO module")
  eco.unlock()

  print("Program ECO module bootloader with OpenOCD")
  output = eco.flash_bootloader(False) # do not use the dummy bootloader
  print("OpenOCD output:")
  print(output)

  print("Program ECO module main flash with OpenOCD")
  output = eco.program()
  print("OpenOCD output:")
  print(output)

  print("Reset ECO Processor with a Pin Reset")
  eco.reset()

  print("Turn ECO LED On")
  success = eco.led_on()
  print("Success" if success else "Failed")
  
  print("Turn ECO LED Off")
  success = eco.led_off()
  print("Success" if success else "Failed")

  print("Turn ECO ECO Processor Off - Pin Reset Required to Wake")
  success = eco.power_off()
  print("Success" if success else "Failed")

  print("Reset ECO Processor with a Pin Reset")
  eco.reset()

  print("Turn ECO LED On")
  success = eco.led_on()
  print("Success" if success else "Failed")
  
  print("Turn ECO LED Off")
  success = eco.led_off()
  print("Success" if success else "Failed")

  print("Create RTC Object")
  rtc = RTC()

  print("Open connection to RTC - first takes control from Linux")
  rtc.open_conn()

  print("Read time from RTC")
  dt = rtc.read_time()
  print("rtc time (UTC):")
  print(dt)
  print("linux time (UTC):")
  print(datetime.now(timezone.utc))

  print("Set current linux time to RTC")
  rtc.set_time(datetime.now(timezone.utc))

  print("Read time from RTC")
  dt = rtc.read_time()
  print("rtc time (UTC):")
  print(dt)
  print("linux time (UTC):")
  print(datetime.now(timezone.utc))

  print("Set RTC alarm IRQ for 30 seconds from now")
  rtc.set_eco_irq_secs(30)

  print("Read alarm time from RTC")
  adt = rtc.read_alarm_time()
  print("rtc alarm time (UTC):")
  print(adt)
  print("linux time now (UTC):")
  print(datetime.now(timezone.utc))

  print("Close RTC connection - do not pass control back to Linux")
  rtc.close_conn(False)  # do not pass control back to linux kernel after closing

  print("Command ECO to cut power from the RPI in 5 seconds")
  print("NOTE: If device does not power off - check Jumper JP2 is installed")
  success  = eco.enter_eco_sleep(5)
  print("Success" if success else "Failed")

  print("Shut down main RPI Processor Safely from code")
  core.shutdown_rpi()
  print("Waiting for ECO to Cut and then Reapply Power to RPI")
