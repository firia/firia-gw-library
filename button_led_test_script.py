from time import sleep

from firiaxg1.core import init_core
from firiaxg1.led import *
from firiaxg1.button import *

def btn_pressed(btn_sel):
  print("Button: {} pressed".format(str(btn_sel)))
  if btn_sel == BTN_SEL.BTN_A:
    blue_led.toggle()
  else:
    green_led.toggle()

if __name__ == "__main__":

  print("Starting test")
  print("Initializing core")
  init_core()

  print("Setting up LEDs")
  red_led = LED(LED_SEL.RED)
  green_led = LED(LED_SEL.GREEN)
  blue_led = LED(LED_SEL.BLUE)
  led_master = LEDMaster()

  print("Turning LEDs on one at a time")
  red_led.on()
  sleep(1)
  green_led.on()
  sleep(1)
  blue_led.on()
  sleep(1)

  print("Using master control to dim LEDs")
  led_master.set_brightness(75)
  sleep(1)
  led_master.set_brightness(25)
  sleep(1)
  led_master.set_brightness(0)
  sleep(1)

  print("Using individual controls to dim LEDs")
  led_master.set_brightness(100)
  sleep(1)
  red_led.set_brightness(25)
  sleep(1)
  green_led.set_brightness(25)  
  sleep(1)
  blue_led.set_brightness(25)
  sleep(1)

  print("Turning off all LEDs")
  red_led.off()
  blue_led.off()
  green_led.off()

  print("Setting up buttons")
  btn_a = Button(BTN_SEL.BTN_A, btn_pressed) 
  btn_b = Button(BTN_SEL.BTN_B, btn_pressed) 

  print("Running button test for 120 seconds")
  print("Press button A to toggle the blue LED")
  print("Press button B to toggle the green LED")
  print("Press both buttons within 1 second window to exit the test")
  n = 120
  while n > 0:
    n -= 1

    btn_a_pressed = btn_a.was_pressed()
    btn_b_pressed = btn_b.was_pressed()
    if btn_a_pressed and btn_b_pressed:
      print("Both buttons were pressed in the window")
      break

    sleep(1)

  print("Exiting test")



