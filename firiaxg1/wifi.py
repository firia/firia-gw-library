from .gpio import *
import subprocess

class WiFi:
  def __init__(self):
      self.wifiDir = '/opt/firia-gw-library/firiaxg1/wifi-config'

  def setApMode(self):
    # Modify the DriverMode line in wifi-config/params.py
    subprocess.call(['sed', '-i', "s/DriverMode = .*/DriverMode = 'AP'/g", 'params.py'], cwd=self.wifiDir)
  
  def setStaMode(self):
    # Modify the DriverMode line in wifi-config/params.py
    subprocess.call(['sed', '-i', "s/DriverMode = .*/DriverMode = 'STA'/g", 'params.py'], cwd=self.wifiDir)

  def setAntennaExt(self):
    # Modify the Antenna line in wifi-config/params.py
    subprocess.call(['sed', '-i', "s/Antenna = .*/Antenna = 'EXT'/g", 'params.py'], cwd=self.wifiDir)

  def setAntennaInt(self):
    # Modify the Antenna line in wifi-config/params.py
    subprocess.call(['sed', '-i', "s/Antenna = .*/Antenna = 'INT'/g", 'params.py'], cwd=self.wifiDir)

  def setApFreq2GHz(self):
    # Modify the hw_mode and channel lines in wifi-config/hostapd.conf
    subprocess.call(['sed', '-i', "s/hw_mode=.*/hw_mode=g/g", 'hostapd.conf'], cwd=self.wifiDir)
    subprocess.call(['sed', '-i', "s/channel=.*/channel=11/g", 'hostapd.conf'], cwd=self.wifiDir)

  def setApFreq5GHz(self):
    # Modify the hw_mode and channel lines in wifi-config/hostapd.conf
    subprocess.call(['sed', '-i', "s/hw_mode=.*/hw_mode=a/g", 'hostapd.conf'], cwd=self.wifiDir)
    subprocess.call(['sed', '-i', "s/channel=.*/channel=36/g", 'hostapd.conf'], cwd=self.wifiDir)

  def setApSsid(self, ssid, password):
    # Ensure the password is at least 8 characters
    if len(password) < 8:
      print('Password must be at least 8 characters')
      return

    # Assign ssid and wpa_passphrase
    subprocess.call(['sed', '-i', f's/^ssid=.*/ssid={ssid}/g', 'hostapd.conf'], cwd=self.wifiDir)
    subprocess.call(['sed', '-i', f's/wpa_passphrase=.*/wpa_passphrase={password}/g', 'hostapd.conf'], cwd=self.wifiDir)

  def setApSubnet(self, subnet):
    # Ensure that the subnet is a #.#.# format
    parts = subnet.split('.')
    if len(parts) != 3:
      print('Invalid subnet')
      return
    
    try:
      if int(parts[0]) > 255 or int(parts[1]) > 255 or int(parts[2]) > 255:
        print('Invalid subnet')
        return
    except ValueError:
      print('Invalid subnet')
      return

    # If we got here, let's assign the params
    subprocess.call(['sed', '-i', f"s/AP_Subnet = .*/AP_Subnet = '{subnet}'/g", 'params.py'], cwd=self.wifiDir)



  def setWifiCredentials(self, ssid, password):
    # Write the SSID and password in the wpa_supplicant file, then 
    # copy the new file to /etc/wpa_supplicant
    subprocess.call(['sed', '-i', f's/^  ssid=.*/  ssid="{ssid}"/g', 'wpa_supplicant.conf'], cwd=self.wifiDir)
    subprocess.call(['sed', '-i', f's/^  psk=.*/  psk="{password}"/g', 'wpa_supplicant.conf'], cwd=self.wifiDir)
    
    subprocess.call(['sudo', 'cp', f'{self.wifiDir}/wpa_supplicant.conf', '/etc/wpa_supplicant'])

  def resetService(self):
    subprocess.call(['sudo', 'systemctl', 'restart', 'redpine.service'])

