from os import system


from .gpio import init_gpio
from .led import init_leds


def init_core():
  init_gpio()
  init_leds()

def shutdown_rpi():
  # add calls here to shutdown cell modem and WiFi properly
  system("sudo shutdown -h now")