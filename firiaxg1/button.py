from enum import IntEnum
from .gpio import *


class BTN_SEL(IntEnum):
  BTN_A = IO_PIN.BTN_A
  BTN_B = IO_PIN.BTN_B


class Button:
  # cb is a function with format cb(btn_sel) that gets called when button is pressed
  #   if cb is not provided then the was_pressed or is_pressed functions must be used
  def __init__(self, btn_sel, cb=None):
    self.pin = btn_sel
    self.cb = cb
    self.was_pressed_flag = False
    init_in_pin(self.pin, pull_up=True, cb=self.handle_press, cb_falling_edge=True)

  # internal handle of a button press - will trigger the cb
  def handle_press(self, chnl):
    self.was_pressed_flag = True
    if self.cb != None:
      self.cb(self.pin)

  # returns whether the button was pressed since the last read of this function
  def was_pressed(self):
    if self.was_pressed_flag:
      self.was_pressed_flag = False
      return True
    return False

  # returns whether the button is currently pressed
  def is_pressed(self):
    return not read_pin(self.pin)
