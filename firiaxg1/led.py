from enum import IntEnum
from .gpio import *


class LED_SEL(IntEnum):
  RED = IO_PIN.LED_IND0
  GREEN = IO_PIN.LED_IND1
  BLUE = IO_PIN.LED_IND2


class LED:
  def __init__(self, led_sel):
    self.pin = led_sel
    self.pwm_mode = False
    self.pwm = None
    self.curr_state = 0
    init_out_pin(self.pin, 0)

  # pcnt (integer 0 to 100) turn LED into PWM and set pcnt
  def set_brightness(self, pcnt):
    self.curr_state = pcnt
    if self.pwm == None:
      self.pwm = get_pwm(self.pin, duty_cycle=pcnt)
    elif self.pwm_mode:
      self.pwm.ChangeDutyCycle(pcnt)
    else:
      self.pwm.start(pcnt)
    self.pwm_mode = True

  def stop_pwm(self):
    if self.pwm_mode:
      self.pwm.stop()
      self.pwm_mode = False

  # turn LED on full bright
  def on(self):
    self.stop_pwm()
    self.curr_state = 100
    set_pin(self.pin, 1)

  def off(self):
    self.stop_pwm()
    self.curr_state = 0
    set_pin(self.pin, 0)

  def toggle(self):
    if self.curr_state == 0:
      self.on()
    else:
      self.off()


class LEDMaster:
  def __init__(self):
    self.pwm = get_pwm(IO_PIN.LED_IND_PWM, freq_hz=1000, duty_cycle=100)

  def set_brightness(self, pcnt):
    self.pwm.ChangeDutyCycle(pcnt)

  
def init_leds():
  init_out_pin(LED_SEL.RED, 0)
  init_out_pin(LED_SEL.GREEN, 0)
  init_out_pin(LED_SEL.BLUE, 0)
  init_out_pin(IO_PIN.LED_IND_PWM, 1)
