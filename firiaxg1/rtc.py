from smbus2 import SMBus
from os import system
from time import sleep
from datetime import datetime, timezone, timedelta

ABX8XX_I2C_ADDR = 0x69  # Address of AB0805 Device on the I2C Bus

# Abracon Registers
ABX8XX_REG_HNDTH = 0x00
ABX8XX_REG_SEC = 0x01
ABX8XX_REG_MIN = 0x02
ABX8XX_REG_HOUR = 0x03
ABX8XX_REG_DATE = 0x04
ABX8XX_REG_MONTH = 0x05
ABX8XX_REG_YEAR = 0x06
ABX8XX_REG_WEEKDAY = 0x07

ABX8XX_REG_ALARM_HNDTH = 0x08
ABX8XX_REG_ALARM_SEC = 0x09
ABX8XX_REG_ALARM_MIN = 0x0A
ABX8XX_REG_ALARM_HOUR = 0x0B
ABX8XX_REG_ALARM_DATE = 0x0C
ABX8XX_REG_ALARM_MONTH = 0x0D
ABX8XX_REG_ALARM_WEEKDAY = 0x0E

ABX8XX_REG_STATUS = 0x0F
ABX8XX_STATUS_AF = 1 << 2
ABX8XX_STATUS_BLF = 1 << 4
ABX8XX_STATUS_WDT = 1 << 6

ABX8XX_REG_CTRL2 = 0x11
ABX8XX_NIRQ_PIN_AIE = 0x03

ABX8XX_REG_IRQ = 0x12
ABX8XX_IRQ_AIE = 1 << 2
ABX8XX_IRQ_IM_1_4	= 0x03 << 5

ABX8XX_REG_OSS = 0x1D
ABX8XX_OSS_OF = 1 << 1
ABX8XX_OSS_OMODE = 4 << 1

class RTC:
  def __init__(self, century=2000):
    self.i2c_smbus = SMBus()
    self.is_linux_control = True
    self.century = century

  def read_time(self):
    rtc_tm = self.i2c_smbus.read_i2c_block_data(ABX8XX_I2C_ADDR, ABX8XX_REG_HNDTH, 8)
    return RTC.rtc2dt(rtc_tm, self.century)

  def set_time(self, utc_date_time):
    buf = RTC.dt2rtc(utc_date_time, self.century)
    err = self.i2c_smbus.write_i2c_block_data(ABX8XX_I2C_ADDR, ABX8XX_REG_HNDTH, buf)

  def give_cntl_of_device_to_linux(self):
    if not self.is_linux_control:
      sleep(2)
      # give control back to the linux kernel
      system('sudo modprobe -r i2c-dev')
      system('sudo modprobe rtc-abx80x')
      self.is_linux_control = True

  def take_cntl_of_device(self):
    if self.is_linux_control:
      # take control from linux kernel
      system('sudo modprobe -r rtc-abx80x')
      # enable dev control of the i2c bus
      system('sudo modprobe i2c-dev')
      self.is_linux_control = False

  def close_conn(self, pass_cntl_to_linux=False):
    self.i2c_smbus.close()
    if pass_cntl_to_linux:
      self.give_cntl_of_device_to_linux()

  def open_conn(self):
    self.take_cntl_of_device()
    self.i2c_smbus.close()
    self.i2c_smbus.open(0)

  def enable_alarm_irq(self):
    err = self.i2c_smbus.write_byte_data(ABX8XX_I2C_ADDR, ABX8XX_REG_IRQ, (ABX8XX_IRQ_IM_1_4 | ABX8XX_IRQ_AIE))
    err2 = self.i2c_smbus.write_byte_data(ABX8XX_I2C_ADDR, ABX8XX_REG_CTRL2, ABX8XX_NIRQ_PIN_AIE)
		  
  def disable_alarm_irq(self):
    err = self.i2c_smbus.write_byte_data(ABX8XX_I2C_ADDR, ABX8XX_REG_IRQ, ABX8XX_IRQ_IM_1_4)

  def read_alarm_time(self):
    rtc_tm = self.i2c_smbus.read_i2c_block_data(ABX8XX_I2C_ADDR, ABX8XX_REG_ALARM_HNDTH, 6)
    return RTC.rtc2dt(rtc_tm, self.century, True)

  def set_eco_irq_time(self, utc_date_time):
    # the alarm is capable of much more... it can be configured to wake on a day of the week
    buf = RTC.dt2rtc(utc_date_time, self.century, True)
    err = self.i2c_smbus.write_i2c_block_data(ABX8XX_I2C_ADDR, ABX8XX_REG_ALARM_HNDTH, buf)
    self.enable_alarm_irq()

  def set_eco_irq_secs(self, time_secs):
    dt_rtc_now = self.read_time()
    dt_alarm = dt_rtc_now + timedelta(seconds=time_secs)
    self.set_eco_irq_time(dt_alarm)

  @staticmethod
  def bcd2bin(value):
    return (value or 0) - 6 * ((value or 0) >> 4)

  @staticmethod
  def bin2bcd(value):
    return (value or 0) + 6 * ((value or 0) // 10)

  @staticmethod
  def rtc2dt(bytes_list, century, is_alarm=False):
    tm_us = RTC.bcd2bin(bytes_list[ABX8XX_REG_HNDTH]) * 10000
    tm_sec = RTC.bcd2bin(bytes_list[ABX8XX_REG_SEC] & 0x7F)
    tm_min = RTC.bcd2bin(bytes_list[ABX8XX_REG_MIN] & 0x7F)
    tm_hour = RTC.bcd2bin(bytes_list[ABX8XX_REG_HOUR] & 0x3F)
    tm_mday = RTC.bcd2bin(bytes_list[ABX8XX_REG_DATE] & 0x3F)
    tm_mon = RTC.bcd2bin(bytes_list[ABX8XX_REG_MONTH] & 0x1F)
    if is_alarm:
      tm_year = datetime.now(timezone.utc).year
    else:
      tm_year = RTC.bcd2bin(bytes_list[ABX8XX_REG_YEAR]) + century
      tm_wday = bytes_list[ABX8XX_REG_WEEKDAY] & 0x7
    dt = datetime(tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec, tm_us, timezone.utc)
    return dt

  @staticmethod
  def dt2rtc(utc_dt, century, is_alarm=False):
    tm_hdth = RTC.bin2bcd(int(utc_dt.microsecond / 10000))
    tm_sec = RTC.bin2bcd(utc_dt.second)
    tm_min = RTC.bin2bcd(utc_dt.minute)
    tm_hour = RTC.bin2bcd(utc_dt.hour)
    tm_wday = utc_dt.weekday()
    tm_mday = RTC.bin2bcd(utc_dt.day)
    tm_mon = RTC.bin2bcd(utc_dt.month)
    tm_year = RTC.bin2bcd(utc_dt.year - century)
    if is_alarm:
      return [tm_hdth, tm_sec, tm_min, tm_hour, tm_mday, tm_mon]
    else:
      return [tm_hdth, tm_sec, tm_min, tm_hour, tm_mday, tm_mon, tm_year, tm_wday]
