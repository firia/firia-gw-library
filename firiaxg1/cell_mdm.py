from time import sleep

import serial
import serial.tools.list_ports

from .gpio import init_out_pin, set_pin, IO_PIN

# the actions performed within this script were all tested on a 
#   NimbeLink SKYWIRE P/N: NL-SW-L TE-TC4NAG cellular modem with soldered down SIM interface
class CellModem:
  def __init__(self, telit_vid=0x1bc7):
    self.vid = telit_vid
    self.ser_conn = None
    init_out_pin(IO_PIN.CELL_ON_OFF, 0)
    init_out_pin(IO_PIN.CELL_RESET, 0)

  def send_at_cmd(self, cmd):
    pass

  def open_at_port(self):
    at_serial_port = self.find_at_port()
    self.ser_conn = serial.Serial(at_serial_port, 115200)

  def close_at_port(self):
    if self.ser_conn is not None and self.ser_conn.is_open:
      self.ser_conn.close()

  def send_at_cmd(self, byte_cmd):
    self.ser_conn.write(bytes(byte_cmd))

  def read_at_resp(self):
    bytes_available = self.ser_conn.in_waiting
    data = self.ser_conn.read(bytes_available)
    return data

  def find_at_port(self):
    at_port = None
    for cp in serial.tools.list_ports.comports():
      if cp.vid == self.vid: 
        try:
          ser = serial.Serial(cp.device, 115200)
          
          if ser.dsr:
            at_port = cp.device

          ser.close()
        except:
          print("Error opening or closing serial port")
            
        if at_port is not None:
          return at_port

    return at_port

  def power_on(self):
    set_pin(IO_PIN.CELL_ON_OFF, 1)
    sleep(1.0)  # To initiate the startup procedure, tie ON_OFF to ground for at least 1 second.
    set_pin(IO_PIN.CELL_ON_OFF, 0) # release the ON_OFF pin to allow activation of the shut down cmd

  def normal_shut_down(self):
    '''
    Shutdown will typically take around 15 seconds for a controlled shutdown.
    Holding the Power pin low will cause the cell modem to immediately start up after a power
    off andd prevent it from being able to shut down.
    If the modem attempts to boot up while power is being removed it may suffer
    irreversible firmware corruption. Users are recommended to actively control this signal.
    '''
    set_pin(IO_PIN.CELL_ON_OFF, 1)
    sleep(2.5) # To power down the modem, tie the ON_OFF pin to ground for at least 2.5 seconds.
    set_pin(IO_PIN.CELL_ON_OFF, 0) # release the ON_OFF pin to prevent immediate startup after power down

  def unconditional_shut_down(self):
    '''
    In the event that the modem becomes unresponsive, pin 5 (nRESET) can be
    grounded to unconditionally shut down the modem. When pin 5 is grounded, the
    modem will cease all ongoing operations and unconditionally shut down. The modem
    will need the ON_OFF signal applied again to power on after a nRESET condition.
    '''
    set_pin(IO_PIN.CELL_RESET, 1)
    sleep(0.2) # assert a logic-low level on nRESET for at least 200 ms
    set_pin(IO_PIN.CELL_RESET, 0)