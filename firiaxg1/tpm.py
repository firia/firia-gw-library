from .spi import SPI, CS_PIN
from .gpio import IO_PIN


class TPM:
  def __init__(self):
    self.spi = SPI(CS_PIN.TPM)

  def check_key(self, secret):
    pass

  def store_key(self, secret):
    pass