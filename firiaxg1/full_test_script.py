import threading
import requests
import json
import serial
import re

from subprocess import Popen, PIPE

import firiaxg1.core as core
from firiaxg1.wifi import WiFi

from firiaxg1.led import *
from firiaxg1.button import *
from firiaxg1.rtc import *
from firiaxg1.eco import *

from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)

print("Starting WiFi test script")
print("Initializing library core")
core.init_core()

print("Construct ECO Processor Object")
eco = ECOProcessor(ECO_MODULE.MGM12P)

print("Turn green LED On")
green_led = LED(LED_SEL.GREEN)
green_led.on()

print("Turn red LED On")
red_led = LED(LED_SEL.RED)
red_led.on()

print("Turn blue LED On")
blue_led = LED(LED_SEL.BLUE)
blue_led.on()

led_master = LEDMaster()

rtc = RTC()

@app.route("/toggle", methods=['POST', 'GET'])
def toggle():
  print("Toggling blue LED")
  blue_led.toggle()
  return json.dumps({'success': True})

def get_intfc_up(intfc):
  cmd = "cat /sys/class/net/{}/operstate".format(intfc)
  pr = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE, bufsize=256)
  ethup = pr.communicate()
  eu = ethup[0].decode('utf-8')
  eu = eu.lower()
  eu = eu.replace(' ', '')
  eu = eu.replace('\n', '')
  e_up = eu == "up"
  return e_up

def check_cp2102_usb():
  cmd = 'lsusb'
  pr = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE, bufsize=256)
  botusbresp = pr.communicate()
  std_out_text = botusbresp[0].decode('utf-8')
  if std_out_text != '':
    idx = std_out_text.find('CP2102')
    if idx != -1:
      return True
  return False

def check_usb_mass_storage():
  pr = Popen(["dmesg", "-t"], shell=False, stdout=PIPE, stderr=PIPE, bufsize=4096)
  botusb_resp = pr.communicate()
  std_out_text = botusb_resp[0].decode('utf-8')
  if std_out_text != '':
    idx = std_out_text.find('USB Mass Storage')
    if idx != -1:
      return True
  return False

def get_eth_ip():
  cmd = 'ip addr show eth0'
  pr = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE, bufsize=256)
  ethmac = pr.communicate()
  std_out_text = ethmac[0].decode('utf-8')
  if std_out_text != '':
    idx = std_out_text.find('inet ')
    if idx != -1:
      close_eth_ip = (std_out_text[idx + 5: idx + 25]).strip()
      eth_words = close_eth_ip.split()
      eth_ip_with24 = eth_words[0]
      eth_words2 = eth_ip_with24.split('/')
      eth_ip = eth_words2[0]
      print("Ethernet IP Address: {}".format(eth_ip))
      return eth_ip
  return ""

def setup_wifi_ap_mode(unique_id):
    print("Construct WiFi Object")
    wifi = WiFi()

    print("Setting WiFi mode as AP")
    wifi.setApMode()

    print("Configure AP SSID and Password")
    ap_ssid = "Kasta-{}".format(unique_id)
    wifi.setApSsid(ap_ssid, "FiriaPassword123!")

    print("Setting WiFi AP as 2.4 GHz")
    wifi.setApFreq2GHz()

    print("Setting AP Subnet")
    wifi.setApSubnet("192.168.15")

    print("Restarting WiFi Service")
    wifi.resetService()

def test_rtc():
  try:
    print("Open connection to RTC - first takes control from Linux")
    rtc.open_conn()
    print("Check RTC Chip Type")
    id_bytes = rtc.i2c_smbus.read_i2c_block_data(ABX8XX_I2C_ADDR, 0x28, 1)
    if id_bytes[0] == 0x08:
      return True
    print("Bad ID Read from RTC")
  except:
    print("Error opening connection to RTC")
  return False

def test_rtc_interrupt():
  try:
    rtc.set_time(datetime.now(timezone.utc))
    print("Setting RTC IRQ time for 12 seconds")
    rtc.set_eco_irq_secs(11)
    print("Putting MGM to sleep in 3 seconds")
    success  = eco.enter_eco_sleep(3)
    sleep(6)
    success = eco.led_on()
    if success:
      return False
    sleep(10)
    success = eco.led_on()
    return success
  except:
    return False

def test_mgm_there_and_program():
  print("Unlocking MGM Module")
  eco.unlock()
  print("Check flash size with OpenOCD")
  output = eco.check_flash_size()
  print("OpenOCD output:")
  full_str1 = ""
  full_str2 = ""
  if output[0] != None:
    full_str1 = output[1].decode('utf-8')
  if output[1] != None:
    full_str2 = output[1].decode('utf-8')
  pat = 'flash size = 1024'
  match1e = re.search(pat, full_str1)
  match1g = re.search(pat, full_str2)
  pat2 = 'flash size = 512'
  match2e = re.search(pat2, full_str1)
  match2g = re.search(pat2, full_str2)
  if match1e != None or match1g != None:
    print("Flash size is 1024 - MGM12P")
    eco.eco_module = ECO_MODULE.MGM12P
  elif match2e != None or match2g != None:
    print("Flash size is 512 - MGM13P")
    eco.eco_module = ECO_MODULE.MGM13P
  else:
    return False

  print("Program ECO module main flash with OpenOCD")
  output = eco.program()
  print("Reset ECO Processor with a Pin Reset")
  eco.reset()
  return True

def get_unique_id_from_rtc():
  id_bytes = rtc.i2c_smbus.read_i2c_block_data(ABX8XX_I2C_ADDR, 0x28, 7)
  id_bytes[6] = id_bytes[6] & 0xFC
  unique_id_str = ''
  for id_byte in id_bytes:
    hex_str = ('%X' % id_byte).rjust(2, '0')
    unique_id_str = unique_id_str + hex_str
  print("RTC Unique ID:", unique_id_str)
  return unique_id_str

def test_tpm():
  cmd = 'sudo /opt/eltt2 -G 8'
  pr = Popen(cmd.split(), shell=False, stdout=PIPE, stderr=PIPE, bufsize=256)
  tpm_return = pr.communicate()
  std_out_text = tpm_return[0].decode('utf-8')
  if std_out_text != '':
    print("TPM Return:", std_out_text)
    idx = std_out_text.find('Random value:')
    if idx != -1:
      return True
  return False

def test_xbee_comms():
  print("Testing XBEE Comms")
  data = b''
  try:
    ser = serial.Serial('/dev/ttyUSB0', baudrate=115200, timeout=2)
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    print("Write to XBEE Serial Port")
    ser.write(b'\x55')
    ser.write(b'\xAA')
    ser.write(b'\x00')
    ser.write(b'\x9D')
    ser.write(b'\x00')
    ser.write(b'\x00')
    ser.write(b'\x9C')
    data = ser.read(10)
    print("Read XBEE version done. Data:", data)
    ser.close()
  except Exception as e:
    print("Error with XBEE:", e)
    return ''

  try:
    ser.close()
  except:
    pass

  if len(data) < 10:
    print("Data was not long enough")
    return ''

  if data[:1] == b'\x55' and data[1:2] == b'\xAA' and data[3:4] == b'\x9D':
    ver = str(int.from_bytes(data[6:7], "big")) + '.'
    ver = ver + str(int.from_bytes(data[7:8], "big")) + '.'
    return ver + str(int.from_bytes(data[8:9], "big"))

  print("Unknown error with XBEE")
  return ''

def main_thread():
  print("READ FILE TO GET CONFIG NOT IMPLEMENTED")
  color = 0
  station_ip = "192.168.8.184"

  pr = Popen(["sudo", "dmesg", "-C"], shell=False, stdout=PIPE, stderr=PIPE, bufsize=256)
  x = pr.communicate()

  other = {
    "brightness": 100,
    "adebounce": False,
    "bdebounce": False,
    "station_ip": station_ip,
  }

  vals = {
    "mac": None,
    "color": color,
    "ipaddr": "",
    "wifi": 0,
    "eth": 0,
    "xbee": 0,
    "btna": 0,
    "btnb": 0,
    "tpm": 0,
    "rtc": 0,
    "mgmspi": 0,
    "mgmsw": 0,
    "topusb": 0,
    "botusb": 0,
    "xbeever": '',
    "rtcint": 0,
  }

  def btn_a_pressed(btn_sel):
    if not other["adebounce"]:
      other["adebounce"] = True
      print("Button A Pressed")
      vals["btna"] = 2
      if other["brightness"] == 100:
        other["brightness"] = 5
      else:
        other["brightness"] = 100
      led_master.set_brightness(other["brightness"])

  def btn_b_pressed(btn_sel):
    if not other["bdebounce"]:
      other["bdebounce"] = True
      print("Button B Pressed")
      vals["btnb"] = 2
      red_led.toggle()

  print("Setting up buttons")
  btn_a = Button(BTN_SEL.BTN_A, btn_a_pressed)
  btn_b = Button(BTN_SEL.BTN_B, btn_b_pressed)

  wifi_setup = False

  while True:
    other["adebounce"] = False
    other["bdebounce"] = False
    if get_intfc_up('eth0'):
      vals["eth"] = 2
    else:
      vals["eth"] = 1

    if vals["eth"] == 2 and vals["mac"] == None:
      print("eth0 is Active: True")
      if test_rtc():
        vals["rtc"] = 2
        vals["mac"] = get_unique_id_from_rtc()
      else:
        vals["rtc"] = 1
      vals["ipaddr"] = get_eth_ip()

    if vals["mac"] != None and not wifi_setup:
      wifi_setup = True
      setup_wifi_ap_mode(vals["mac"])

    if vals["tpm"] == 0 or vals["tpm"] == 1:
      if test_tpm():
        vals["tpm"] = 2
      else:
        vals["tpm"] = 1

    if vals["xbee"] == 0 or vals["xbee"] == 1:
      vals["xbeever"] = test_xbee_comms()
      if vals["xbeever"] != '':
        vals["xbee"] = 2
      else:
        vals["xbee"] = 1

    if vals["mgmsw"] == 0 or vals["mgmsw"] == 1:
      if test_mgm_there_and_program():
        vals["mgmsw"] = 2
      else:
        vals["mgmsw"] = 1

    if vals["mgmsw"] == 2 and (vals["mgmspi"] == 0 or vals["mgmspi"] == 1):
      success = False
      try:
        print("Turn ECO LED On Get Ack")
        success = eco.led_on()
      except:
        pass

      if success:
        vals["mgmspi"] = 2
      else:
        vals["mgmspi"] = 1

    if vals["rtc"] == 2 and vals["mgmspi"] == 2 and (vals["rtcint"] == 0 or vals["rtcint"] == 1):
      if test_rtc_interrupt():
        vals["rtcint"] = 2
      else:
        vals["rtcint"] = 1

    if vals["topusb"] == 0 or vals["topusb"] == 1:
      if check_usb_mass_storage():
        vals["topusb"] = 2

    if vals["botusb"] == 0 or vals["botusb"] == 1:
      if check_cp2102_usb():
        vals["botusb"] = 2

    if wifi_setup and (vals["wifi"] == 0 or vals["wifi"] == 1):
      if get_intfc_up('wlan0'):
        print("wlan0 is Active: True")
        vals["wifi"] = 2
      else:
        vals["wifi"] = 1

    if vals["eth"] == 2:
      url = "http://" + other["station_ip"] + ':80/status'
      headers = {'Content-type': 'application/json', 'Accept': 'text/plain', 'Origin': '*', 'Access-Control-Request-Headers': 'Authorization'}
      reset_mac = False
      try:
        if vals["mac"] == None:
          vals["mac"] = '%X' % color
          reset_mac = True
        resp = requests.post(url, data=json.dumps(vals), headers=headers, timeout=2.0, verify=False)
        if resp.status_code == 200:
          print("Status sent, resp good")
        else:
          print("Bad transfer of status")
      except:
        print("No server to get status")

      if reset_mac:
        vals["mac"] = None

    sleep(3)

if __name__ == "__main__":
  main_th = threading.Thread(target=main_thread)
  main_th.start()
  app.run(debug=False, port=80, host='0.0.0.0')