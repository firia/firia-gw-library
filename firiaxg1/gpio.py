import RPi.GPIO as GPIO
from enum import IntEnum


# The following are GPIO numbers based on xg1-Sch-RevA.pdf dated 2020-04-08
class IO_PIN(IntEnum):
  I2C_SDA0 = 0  # RTC, ECO
  I2C_SCL0 = 1  # RTC, ECO
  GPIO2 = 2  # Expansion Port PIN 4
  GPIO3 = 3  # Expansion Port PIN 6
  LED_IND0 = 4  # Red LED
  LED_IND1 = 5  # Green LED
  LED_IND2 = 6  # Blue LED
  SPI0_CE1_N = 7  # TPM Chip Select
  SPI0_CE0_N = 8  # ECO Chip Select (TP7)
  SPI0_MISO = 9  # ECO, XBEE, TP8
  SPI0_MOSI = 10  # ECO, XBEE, TP9
  SPI0_CLK = 11  # ECO, XBEE, TP10
  GPIO12 = 12  # Expansion Port PIN 9
  GPIO13 = 13  # Expansion Port PIN 8
  LP_TXD0 = 14  # TP20
  LP_RXD0 = 15  # TP4
  LP_CTS0 = 16
  LP_RTS0 = 17
  GPIO18 = 18  # Expansion Port PIN 1
  GPIO19 = 19  # Expansion Port PIN 3
  GPIO20 = 20  # Expansion Port PIN 5
  GPIO21 = 21  # Expansion Port PIN 7
  MGM_SWCLK = 22  # Program ECO with OpenOCD
  MGM_SWO = 23  # Program ECO with OpenOCD
  MGM_SWDIO = 24  # Program ECO with OpenOCD
  MGM_RESET = 25
  XBEE_RESET = 26
  RF_SYNC = 27  # ECO, XBEE, CELL
  BTN_A = 28  # Active Low
  BTN_B = 29  # Active Low
  CON_CTS1 = 30  # USB Port / Serial Console
  CON_RTS1 = 31  # USB Port / Serial Console
  CON_TXD1 = 32  # USB Port / Serial Console
  CON_RXD1 = 33  # USB Port / Serial Console
  SD1_CLK = 34
  SD1_CMD = 35
  SD1_DAT0 = 36
  SD1_DAT1 = 37
  SD1_DAT2 = 38
  SD1_DAT3 = 39
  WIFI_POC_IN = 40  # Bring High First on Boot
  WIFI_RESET = 41  # Bring High Second on Boot
  EN_XBEE_SPI = 42  # XBEE SPI0 Chip Select
  CELL_ON_OFF = 43
  CELL_RESET = 44
  LED_IND_PWM = 45


# init_val (int) 0 or 1 
def init_out_pin(pin, init_val=None):
  GPIO.setup(pin, GPIO.OUT)
  if init_val != None:
    GPIO.output(pin, init_val)


# val (int) 0 or 1
def set_pin(pin, val):
  GPIO.output(pin, val)     


# cb will call the provided function with a chnl parameter
def init_in_pin(pin, pull_up=True, cb=None, cb_falling_edge=True):
  pull_ud = GPIO.PUD_UP if pull_up else GPIO.PUD_DOWN
  GPIO.setup(pin, GPIO.IN, pull_up_down=pull_ud)
  if cb is not None:
    cb_on = GPIO.FALLING if cb_falling_edge else GPIO.RISING
    GPIO.add_event_detect(pin, cb_on)
    GPIO.add_event_callback(pin, cb)


def read_pin(pin):
  return GPIO.input(pin)


# duty_cycle (int) 0 to 100 - % ON
# once you have the pwm object you can change the duty cycle with
#       pwm.ChangeDutyCycle(75)   # same as 75% ON
#       pwm.ChangeFrequency(1000)  
def get_pwm(pin, freq_hz=1000, duty_cycle=0):
  init_out_pin(pin)
  pwm = GPIO.PWM(pin, freq_hz)
  pwm.start(duty_cycle)
  return pwm


def init_gpio():
  GPIO.setmode(GPIO.BCM)
  GPIO.setwarnings(False)