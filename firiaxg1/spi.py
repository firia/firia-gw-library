from spidev import SpiDev
from .gpio import *


class SPI_SEL(IntEnum):
  TPM = IO_PIN.SPI0_CE1_N
  ECO = IO_PIN.SPI0_CE0_N
  XBEE = IO_PIN.EN_XBEE_SPI


class SPI:
  def __init__(self, spi_sel, bus=0, max_speed_hz=1000000):
    self.spi = SpiDev()
    self.cs_pin = spi_sel
    self.bus = bus

    self.device = 0
    if self.cs_pin == SPI_SEL.TPM:
      self.device = 1

    self.spi.open(bus, self.device)
    self.spi.max_speed_hz = max_speed_hz  # defaults to 1 MHz

    if self.cs_pin == SPI_SEL.XBEE:
      self.spi.no_cs = True
      init_out_pin(self.cs_pin, 1)

  def set_cs_low(self):
    if self.spi.no_cs:
      set_pin(self.cs_pin, 0)

  def set_cs_high(self):
    if self.spi.no_cs:
      set_pin(self.cs_pin, 1)

  # byte_list (list of 0 - 255 integer values)
  def send_bytes(self, byte_list):
    self.set_cs_low()
    self.spi.xfer2(byte_list)
    self.set_cs_high()

  def read_bytes(self, len_bytes):
    self.set_cs_low()
    rb = self.spi.readbytes(len_bytes)
    self.set_cs_high()
    return rb