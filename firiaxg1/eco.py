from time import sleep
from subprocess import Popen, PIPE
from os import path

from .spi import SPI, SPI_SEL
from .gpio import *

import wiringpi


SPI_FILL_BYTE_1 = 0x2C
SPI_FILL_BYTE_2 = 0x77
SPI_CHECKSUM_BYTE = 0x5C

SPI_ACK_MSG = 0x01
SPI_CMD_LED_ON = 0x02 # turn on LED attached to eco processor
SPI_CMD_LED_OFF = 0x03 # turn off LED attached to eco processor
SPI_CMD_RPI_SLEEP_INT_WAKE = 0x04  # turn off the power on the RPI and resume on interrupt from RTC
SPI_CMD_ECO_OFF = 0x05  # put eco processor in deepest sleep (only wake option is reset)

PROGRAMMER_12P_CFG_FILE = "mgm12p.cfg"
PROGRAMMER_13P_CFG_FILE = "mgm13p.cfg"
PROGRAMMER_12P_DUMMY_BOOTLOADER_CFG_FILE = "mgm12p_dummy_bootloader.cfg"
PROGRAMMER_12P_ECO_BOOTLOADER_CFG_FILE = "mgm12p_eco_bootloader.cfg"
PROGRAMMER_13P_DUMMY_BOOTLOADER_CFG_FILE = "mgm13p_dummy_bootloader.cfg"
PROGRAMMER_13P_ECO_BOOTLOADER_CFG_FILE = "mgm13p_eco_bootloader.cfg"


class EcoUnlocker(object):
  def __init__(self, swdio_pin, swclk_pin, rst_pin):
    self.swdio_pin = swdio_pin
    self.swclk_pin = swclk_pin
    self.rst_pin = rst_pin

    # this uses wiringpi as opposed to RPI.GPIO because wiring pi implements
    # GPIO controls faster and more consistently
    wiringpi.wiringPiSetupGpio()
    wiringpi.pinMode(self.swclk_pin, 1)
    wiringpi.pinMode(self.swdio_pin, 1)
    wiringpi.pinMode(self.rst_pin, 1)
        
    self.unlock_sequence = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,     # Line reset
                            0x9E, 0xE7,                                   # JTAG-to-SWD
                            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,     # Line reset
                            0x00, 0xA5, 0x00, 0x00, 0x00, 0x00, 0x00,     # Read IDCODE
                            0xA9, 0x00, 0x00, 0x00, 0x00, 0x22,           # Write PWRUPREQ to CTRL
                            0xB1, 0x00, 0x00, 0x00, 0x00, 0x00,           # Write 0 to SELECT
                            0x8B, 0x00, 0x23, 0x98, 0xf5, 0x39,           # Write unlock key to AAP_CMDKEY
                            0xA3, 0x20, 0x00, 0x00, 0x00, 0x20,           # Write DEVICEERASE bit to AAP_CMD
                            0x8B, 0x00, 0x00, 0x00, 0x00, 0x00,           # Write 0 to AAP_CMDKEY
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]     # Idle cycles

  def send_unlock_seq(self):
    wiringpi.digitalWrite(self.rst_pin, 1)
    for val in self.unlock_sequence:
      self.write_lsb(val, 8)

  def write_lsb(self, data, num_bits):
    for i in range(0, num_bits):
      b = (data >> i) & 0x01
      self.write_bit(b)

  def write_bit(self, bit):
    wiringpi.digitalWrite(self.swdio_pin, bit)
    self.swclk_cycle() 

  def swclk_cycle(self):
    wiringpi.digitalWrite(self.swclk_pin, 0)
    wiringpi.delayMicroseconds(1)
    wiringpi.digitalWrite(self.swclk_pin, 1)
    wiringpi.delayMicroseconds(1)

class ECO_MODULE(IntEnum):
  MGM12P = 0
  MGM13P = 1

class ECOProcessor:
  def __init__(self, eco_module=ECO_MODULE.MGM12P, programmer_dir=None):
    self.rst_pin = IO_PIN.MGM_RESET

    self.eco_module = eco_module
    if programmer_dir == None:
      self.programmer_dir = path.join(path.dirname(__file__), 'eco-programmer')
    else:
      self.programmer_dir = programmer_dir

    init_out_pin(self.rst_pin, 1)
    init_out_pin(IO_PIN.SPI0_CE0_N, 1)

    self.spi = SPI(SPI_SEL.ECO)

  # this can bitbang the Silabs "unlock" sequence detailed in the EFM32 AN0062 Application Note
  #   it will unlock the flash and cause a mass erase of the flash (does not erase the bootloader section) 
  # this can fail if the dummy bootloader is loaded and the device becomes locked in code immediately
  def unlock(self):
    eco_unlocker = EcoUnlocker(24, 22, 25)
    eco_unlocker.send_unlock_seq()
    sleep(1)

  # hard reset of the ECO programmer through its reset pin
  def reset(self):
    set_pin(self.rst_pin, 0)
    sleep(0.2)
    set_pin(self.rst_pin, 1)
    sleep(1.0)

  def get_ack(self):
    sleep(0.1)
    ack = self.spi.read_bytes(4)
    sleep(0.1)
    return ack[0] == SPI_ACK_MSG and ack[1] == SPI_FILL_BYTE_1 and ack[2] == SPI_FILL_BYTE_2 and ack[3] == SPI_CHECKSUM_BYTE

  def led_on(self):
    cmd = [SPI_CMD_LED_ON, SPI_FILL_BYTE_1, SPI_FILL_BYTE_2, SPI_CHECKSUM_BYTE]
    self.spi.send_bytes(cmd)
    return self.get_ack()

  def led_off(self):
    cmd = [SPI_CMD_LED_OFF, SPI_FILL_BYTE_1, SPI_FILL_BYTE_2, SPI_CHECKSUM_BYTE]
    self.spi.send_bytes(cmd)
    return self.get_ack()

  # hard reset of the ECO programmer through its reset pin
  def enter_eco_sleep(self, delay_before_sleep_secs=10):
    cmd = [SPI_CMD_RPI_SLEEP_INT_WAKE, (delay_before_sleep_secs >> 8) & 0xFF, delay_before_sleep_secs & 0xFF, SPI_CHECKSUM_BYTE]
    self.spi.send_bytes(cmd)
    return self.get_ack()

  # this will effectively shut down the eco device
  # it doesn't actually power off just puts the eco processor into 
  # a min power state where as hard reset is required to exit
  def power_off(self):
    cmd = [SPI_CMD_ECO_OFF, SPI_FILL_BYTE_1, SPI_FILL_BYTE_2, SPI_CHECKSUM_BYTE]
    self.spi.send_bytes(cmd)
    return self.get_ack()

  # this programs the main section in flash starting at 0x00000000 using OpenOCD
  # it will program the main code found built from firia-gw1-eco-code
  def program(self):
    cmd = "sudo openocd -f "
    if self.eco_module == ECO_MODULE.MGM12P:
      cmd += PROGRAMMER_12P_CFG_FILE
    else:
      cmd += PROGRAMMER_13P_CFG_FILE
    pr = Popen(cmd.split(), cwd=self.programmer_dir, shell=False, stderr=PIPE, bufsize=256)
    return pr.communicate()

  # this programs a bootloader to the bootloader section on the MGM module
  # the bootloader section is located at 0xFE10000 memory location
  # the MGM modules come from the factory with a DUMMY bootloader preprogrammed
  # the ECO bootloader can be built from the firia-gw1-eco-code
  #   the ECO bootloader is the silabs Gecko UART bootloader (see UG266) 
  #   and givesthe processor time to mass erase during the boot process
  def flash_bootloader(self, use_dummy=True):
    cmd = "sudo openocd -f "
    if self.eco_module == ECO_MODULE.MGM12P:
      if use_dummy:
        cmd += PROGRAMMER_12P_DUMMY_BOOTLOADER_CFG_FILE
      else:
        cmd += PROGRAMMER_12P_ECO_BOOTLOADER_CFG_FILE  

    elif self.eco_module == ECO_MODULE.MGM13P:
      if use_dummy:
        cmd += PROGRAMMER_13P_DUMMY_BOOTLOADER_CFG_FILE
      else:
        cmd += PROGRAMMER_13P_ECO_BOOTLOADER_CFG_FILE
        
    pr = Popen(cmd.split(), cwd=self.programmer_dir, shell=False, stderr=PIPE, bufsize=256)
    return pr.communicate()      
