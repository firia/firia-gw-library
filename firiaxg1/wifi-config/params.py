# Driver Mode is either 'STA' or 'AP'
DriverMode = 'AP'

# Antenna is either 'INT' or 'EXT'
Antenna = 'EXT'

# Subnet must always be #.#.#
AP_Subnet = '192.168.14'
