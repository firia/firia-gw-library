from params import DriverMode, Antenna, AP_Subnet
import RPi.GPIO as GPIO
import subprocess
import time

def listNetDevices():
	netstat = subprocess.check_output(['netstat', '-i'])
	netstatStr = netstat.decode('utf-8')
	lines = netstatStr.split('\n')
	lines.pop()
	# Burn off the first two lines
	del lines[0]
	del lines[0]
	interfaces = []
	for line in lines:
		# Split by white spaces
		elements = line.split()
		if len(elements) > 0:
			interfaces.append(elements[0])
	# Return the full list of devices
	return set(interfaces)

def getBusInfo(dev):
	# Parse result of ethtool query to get bus-info
	res = subprocess.check_output(['ethtool', '-i', dev])
	resString = res.decode('utf-8')
	lines = resString.split('\n')
	lines.pop()
	for line in lines:
		elements = line.split()
		if elements[0] == 'bus-info:':
			return elements[1]


def getPhyNumber(dev):
	iwdev = subprocess.check_output(['iw', 'dev'])
	iwdevStr = iwdev.decode('utf-8')
	lines = iwdevStr.split('\n')
	# Enumerate lines looking for 'Interface {dev}'
	for index, line in enumerate(lines):
		if f'Interface {dev}' in line:
			phyLine = lines[index-1]
			return phyLine.split('#')[1]

# Remove modules if loaded
subprocess.call(['sudo', 'rmmod', 'rsi_sdio'])
subprocess.call(['sudo', 'rmmod', 'rsi_91x'])
subprocess.call(['sudo', 'modprobe', '-r', 'cfg80211'])
subprocess.call(['sudo', 'modprobe', '-r', 'mac80211'])
subprocess.call(['sudo', 'modprobe', '-r', 'bluetooth'])
subprocess.call(['sudo', 'modprobe', '-r', 'brcmfmac'])

# Enable the Redpine device
print('Enabling the Wifi device')
GPIO.setmode(GPIO.BCM)
GPIO.setup(40, GPIO.OUT)
GPIO.setup(41, GPIO.OUT)
GPIO.output(40, 0)
GPIO.output(41, 0)
time.sleep(1)
GPIO.output(40, 1)
time.sleep(1)
GPIO.output(41, 1)
time.sleep(2)

# Enable necessary modules for Redpine driver
print('Loading kernel modules')
subprocess.call(['sudo', 'modprobe', 'cfg80211'])
subprocess.call(['sudo', 'modprobe', 'mac80211'])
subprocess.call(['sudo', 'modprobe', 'bluetooth'])
subprocess.call(['sudo', 'modprobe', 'brcmfmac'])

# Insert the Redpine modules
print('Inserting Redpine driver modules')
# Insert in STA mode
if DriverMode == 'STA':
    subprocess.call(['sudo', 'systemctl', 'stop', 'hostapd.service'])
    subprocess.call(['sudo', 'insmod', 'rsi_91x.ko', 'rsi_zone_enabled=1', 'dev_oper_mode=13'], cwd='/opt/RS911X.NB0.NL.GNU.LNX.2.0.RC4/rsi')
# Insert in AP mode
elif DriverMode == 'AP':
	subprocess.call(['sudo', 'insmod', 'rsi_91x.ko', 'rsi_zone_enabled=0x1', 'dev_oper_mode=14'], cwd='/opt/RS911X.NB0.NL.GNU.LNX.2.0.RC4/rsi')
# After inserting 91x, insert sdio module
subprocess.call(['sudo', 'insmod', 'rsi_sdio.ko'], cwd='/opt/RS911X.NB0.NL.GNU.LNX.2.0.RC4/rsi')

# Sleep for a bit to allow the devices to enumerate
time.sleep(1)

# Get list of devices after module is inserted
netDevices = listNetDevices()

# Always remove 'lo' because it doesn't like when you query it
netDevices.remove('lo')
print(netDevices)
redpineDev = None
for dev in netDevices:
	bus_info = getBusInfo(dev)
	if bus_info == 'mmc1:fffd:1':
		print(f'Redpine device found: {dev}')
		redpineDev = dev

if not redpineDev:
	print('Failed to find the Redpine device. Uh oh')
	exit()

print('device:' + redpineDev)

# Set the antenna mode
print('Setting antenna mode')
phyNum = getPhyNumber(redpineDev)
subprocess.call(['sudo', 'ifconfig', redpineDev, 'down'])
if Antenna == 'EXT':
	subprocess.call(['sudo', 'iw', 'phy', f'phy{phyNum}', 'set', 'antenna', '1', '0'])
elif Antenna == 'INT':
	subprocess.call(['sudo', 'iw', 'phy', f'phy{phyNum}', 'set', 'antenna', '0', '0'])
subprocess.call(['sudo', 'ifconfig', redpineDev, 'up'])

print('Disabled power save mode')
subprocess.call(['sudo', 'iw', redpineDev, 'set', 'power_save', 'off'])

#### If running in AP mode
# Set the IP address of the interface
if DriverMode == 'AP':
	wlanIP = f'{AP_Subnet}.1'
	netmask = '255.255.255.0'
	wlanBCast = f'{AP_Subnet}.255'
	ipRange = f'{AP_Subnet}.2,{AP_Subnet}.20,255.255.255.0,24h'
	
	# Change the IP address of this interface to the desired IP
	subprocess.call(['sudo', 'ifconfig', redpineDev, wlanIP, 'netmask', netmask, 'broadcast', wlanBCast])
	time.sleep(1)

	# Modify dnamasq.conf with updated subnet
	subprocess.call(['sed', '-i', f's/dhcp-range=.*/dhcp-range={ipRange}/g', '/opt/firia-gw-library/firiaxg1/wifi-config/dnsmasq.conf'])
	subprocess.call(['sed', '-i', f's/address=.*/address=\/gw.wlan\/{wlanIP}/g', '/opt/firia-gw-library/firiaxg1/wifi-config/dnsmasq.conf'])
	subprocess.Popen(['sudo', 'dnsmasq', '--no-daemon', f'--interface={redpineDev}', '--conf-file=/opt/firia-gw-library/firiaxg1/wifi-config/dnsmasq.conf', '--log-queries'])
	time.sleep(1)
	# Start up the AP
	subprocess.Popen(['sudo', 'hostapd', '-i', redpineDev, '/opt/firia-gw-library/firiaxg1/wifi-config/hostapd.conf'])    

time.sleep(1)

if DriverMode == 'AP':
  while True:
    time.sleep(10)
