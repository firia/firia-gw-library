from time import sleep

from datetime import datetime, timezone

import firiaxg1.core as core
from firiaxg1.wifi import WiFi

if __name__ == "__main__":
  print("Starting WiFi test script")
  print("Initializing library core")
  core.init_core()

  print("Construct WiFi Object")
  wifi = WiFi()

  print("Setting WiFi mode as AP")
  wifi.setApMode()

  print("Setting WiFi mode as STA")
  wifi.setStaMode()

  print("Configure AP SSID and Password")
  wifi.setApSsid("FiriaXG1", "FiriaPassword123!")

  print("Setting WiFi AP as 2.4 GHz")
  wifi.setApFreq2GHz()

  print("Setting WiFi AP as 5 GHz")
  wifi.setApFreq5GHz()

  print("Setting AP Subnet")
  wifi.setApSubnet("192.168.15")

  print("Configuration STA SSID and Password")
  #wifi.setWifiCredentials("MyHomeWifi", "MyWifiPassword")

  print("Restarting WiFi Service")
  #wifi.resetService()
